{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.sequoia-dump;
  toEnvironmentCfg = vars:
    (concatStringsSep " "
      (mapAttrsToList (k: v: "${k}=${escapeShellArg v}") vars));
in {
  options.services.sequoia-dump= {
    enable = mkEnableOption "sequoia-dump";

    port = mkOption {
      type = types.port;
      default = 8000;
      description = ''
        Port on which the Dymo printer webservice should listen.
      '';
    };
    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Whether to automatically open the specified ports in the firewall.
      '';
    };

    settings = mkOption {
      type = types.attrsOf (types.oneOf [ types.str types.int ]);
      default = { };
      example = literalExample ''
        {
          ROCKET_LOG = "debug";
        }
      '';
      description = ''
        Settings for sequoia-dump, passed as environment variables.
      '';
    };

  };

  config = lib.mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = if cfg.openFirewall then [ cfg.port ] else [];

    systemd.services.sequoia-dump = {
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.sequoia-dump}/bin/sequoia-dump";
        WorkingDirectory = "${pkgs.sequoia-dump}";
        User = "nobody";
        Environment =
          toEnvironmentCfg ({ ROCKET_PORT = cfg.port; } // cfg.settings);
        MemoryLimit = "512M";
	StandardOutput = "journal";
	StandardError = "journal";
      };
    };
  };
}
