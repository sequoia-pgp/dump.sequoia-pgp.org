{ callPackage, fetchFromGitHub, makeRustPlatform }:

{ date, channel }:

let
  mozillaOverlay = fetchFromGitHub {
      owner = "mozilla";
      repo = "nixpkgs-mozilla";
      rev = "b1001ed670666ca4ce1c1b064481f88694315c1d";
      sha256 = "sha256:1hpig8z4pzdwc2vazr6hg7qyxllbgznsaivaigjnmrdszlxz55zz";
    };
  #mozillaOverlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  mozilla = callPackage "${mozillaOverlay.out}/package-set.nix" {};
  rustSpecific = (mozilla.rustChannelOf { inherit date channel; }).rust;
in makeRustPlatform {
  cargo = rustSpecific;
  rustc = rustSpecific;
}

